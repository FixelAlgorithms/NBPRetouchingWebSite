---
title: "Nino Batista"
image: "images/team/NinoBatista.jpg"
email: "ninobatista@gmail.com"
social:
  - icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
    link : "http://www.facebook.com/ninobatista"
  - icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
    link : "http://www.twitter.com/ninobatista"
  - icon : "ti-instagram" # themify icon pack : https://themify.me/themify-icons
    link : "http://www.instagram.com/ninobatista"
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet vulputate augue. Duis auctor lacus id vehicula gravida. Nam suscipit vitae purus et laoreet.
Donec nisi dolor, consequat vel pretium id, auctor in dui. Nam iaculis, neque ac ullamcorper.