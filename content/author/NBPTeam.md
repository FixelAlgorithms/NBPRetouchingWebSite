---
title: "NBP Team"
image: ""
email: "support@ninobatista.com"
social:
  - icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.facebook.com/nbpretouch/"
  - icon : "ti-instagram" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.instagram.com/nbpretouchtools"
---

What I think.